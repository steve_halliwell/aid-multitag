using UnityEngine;
using UnityEditor;
using System.Collections;


namespace AID{ 
[CustomEditor(typeof(MultiTag))]
    [CanEditMultipleObjects]
    public class MultiTagInspector : Editor {	

	private string tag = "";
	private string messageToUser = "";
	private SerializedProperty _hashes;

	private void OnEnable()
	{
		_hashes = serializedObject.FindProperty("stringHashes");
	}
	
	public override void OnInspectorGUI ()
	{
		MultiTag my = (MultiTag)target;
		
		//EditorGUIUtility.LookLikeControls();
		DrawDefaultInspector();

		serializedObject.Update();

		GUILayout.Space(10f);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Tag name: ");
		tag = GUILayout.TextField(tag);
		GUILayout.EndHorizontal();

		//TODO move everything tothe serialisedproperty

		GUILayout.BeginHorizontal();
		if(GUILayout.Button("Add"))
		{
		//	Undo.RecordObject(my.stringHashes, "Added to multitag " + tag);
			if(!my.HasTag(tag))
			{
				//my.stringHashes.Add(tag.GetHashCode());
				_hashes.InsertArrayElementAtIndex(_hashes.arraySize);
				_hashes.GetArrayElementAtIndex(_hashes.arraySize-1).intValue = tag.GetHashCode();
			}
		}
		if(GUILayout.Button("Remove"))
		{
		//	Undo.RecordObject(my.stringHashes, "Removed from multitag " + tag);
			if(my.HasTag(tag))
			{
				int code = tag.GetHashCode();
				//my.stringHashes.Remove(tag.GetHashCode());
				for(int i = 0 ; i < _hashes.arraySize; i++)
				{
					if(code == _hashes.GetArrayElementAtIndex(i).intValue)
					{
						_hashes.DeleteArrayElementAtIndex(i);
						break;
					}
				}
			}
		}
		if(GUILayout.Button("Check"))
		{
			if(my.HasTag(tag))
			{
				messageToUser = "Yes " + my.gameObject.name + " MultiTag contains; " + tag;
			}
			else
			{
				messageToUser = "No " + my.gameObject.name + " MultiTag does not contain; " + tag;
			}
			
			Debug.Log(messageToUser);
		}
			
		GUILayout.EndHorizontal();
		if(!string.IsNullOrEmpty(messageToUser))
		{
			GUILayout.Space(10);
			GUILayout.Label(messageToUser);
		}

		//TODO add text field and add tag and remove tag buttons
		if(EditorApplication.isPlaying)
		{
			if(GUILayout.Button("Refresh Tags"))
			{
				//lazy, remove from all, add to all
				my.OnDisable();
				my.OnEnable();
			}
		}

		serializedObject.ApplyModifiedProperties();
	}
}
}