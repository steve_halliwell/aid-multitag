﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO
//  does this need to be a monobeh? seems like it could just be a mono state

namespace AID
{
    /*
     * Allows for GameObject.Find* like behaviour for multitags. Be aware that this class returns references to the same internal
     * lists that it keeps thus making changes to that list will potentially damage the multitag search in further calls. If you 
     * need to mess with the returned list (add or remove things) then simply add a GetRange(0, list.count); after the returned list
     */
    public class MultiTagSearch
    {
        public Dictionary<int, List<MultiTag>> groups = new Dictionary<int, List<MultiTag>>();
        public static bool applicationIsClosing = false; //used by multitag to not unlink during a shutdown

        //returns first GO with given tag
        public MultiTag Find(string tag)
        {
            return Find(tag.GetHashCode());
        }

        public MultiTag Find(int tagHash)
        {
            List<MultiTag> list = _findAllWithTag(tagHash);

            if (list != null)
            {
                return list[0];
            }
            else
            {
                return null;
            }
        }

        //return a list of all GO's with tag
        public List<MultiTag> FindAllWithTag(string tag)
        {
            return FindAllWithTag(tag.GetHashCode());
        }

        //internal usage returns null if it doesn't exist
        public List<MultiTag> _findAllWithTag(int tagHash)
        {
            List<MultiTag> list = null;
            groups.TryGetValue(tagHash, out list);
            return list;
        }

        //return a list of all GO's with tag, outward facing returns empty list if not found
        public List<MultiTag> FindAllWithTag(int tagHash)
        {
            List<MultiTag> list = _findAllWithTag(tagHash);
            return (list != null ? list : new List<MultiTag>());
        }


        //internal function that adds an item to the collection
        public void _addItem(int taghash, MultiTag go)
        {
            List<MultiTag> list = _findAllWithTag(taghash);

            if (list == null)
            {
                list = new List<MultiTag>();
                groups.Add(taghash, list);
            }

            list.Add(go);
        }

        public void _addAll(MultiTag go)
        {
            for (int i = 0; i < go.stringHashes.Count; i++)
            {
                _addItem(go.stringHashes[i], go);
            }
        }


        //internal function that removes item from collection
        public void _removeItem(int taghash, MultiTag go)
        {
            List<MultiTag> list = _findAllWithTag(taghash);

            if (list != null)
            {
                list.Remove(go);
            }
        }

        public void _removeAll(MultiTag go)
        {
            for (int i = 0; i < go.stringHashes.Count; i++)
            {
                _removeItem(go.stringHashes[i], go);
            }
        }

        //singleton stuff
        static private MultiTagSearch inst;
        
        //this is how you get access to this classes functions
        static public MultiTagSearch Instance()
        {
            if (inst == null)
                inst = new MultiTagSearch();

            return inst;
        }
    }
}