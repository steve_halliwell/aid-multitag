﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AID
{
    /*
        Provides a unified way to give a GO as many tags as you wish. This is not an 
            optimal solution it is a simple and flexible one.

        This functionality is also merged with the ability to search multitags via the 
            MultiTagSearch singleton.
    */
    public class MultiTag : MonoBehaviour
    {

        [SerializeField]
        public List<int> stringHashes = new List<int>();

        public void AddTag(string newTag)
        {
            AddTag(newTag.GetHashCode());
        }

        public void AddTag(int newTagHash)
        {
            if (!HasTag(newTagHash))
            {
                stringHashes.Add(newTagHash);
                MultiTagSearch.Instance()._addItem(newTagHash, this);
            }
        }

        public bool HasTag(string tag)
        {
            return HasTag(tag.GetHashCode());
        }

        public bool HasTag(int tagHash)
        {
            return stringHashes.Contains(tagHash);
        }

        public void RemoveTag(string oldTag)
        {
            RemoveTag(oldTag.GetHashCode());
        }

        public void RemoveTag(int oldTagHash)
        {
            stringHashes.Remove(oldTagHash);
            MultiTagSearch.Instance()._removeItem(oldTagHash, this);
        }
        
        public void OnEnable()
        {
            int ourGOTagHash = gameObject.tag.GetHashCode();
            if (!HasTag(gameObject.tag))
                stringHashes.Add(ourGOTagHash);

            MultiTagSearch.Instance()._addAll(this);
        }

        public void OnDisable()
        {
            if (MultiTagSearch.applicationIsClosing)
                return;

            MultiTagSearch.Instance()._removeAll(this);
        }
    }
}
