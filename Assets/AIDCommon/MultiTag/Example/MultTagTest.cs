﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultTagTest : MonoBehaviour {

	void Start()
	{
		//find all untagged
        List<AID.MultiTag> untagged = AID.MultiTagSearch.Instance().FindAllWithTag("Untagged".GetHashCode());
		print( "found " + untagged.Count.ToString() + " that are Untagged via hashcode");

		//find all billy
        List<AID.MultiTag> billys = AID.MultiTagSearch.Instance().FindAllWithTag("billy");
		print ("found " + billys.Count.ToString() + " billys");

		int i = 0;
        foreach(AID.MultiTag mt in billys)
		{
			//find the jane
			if(mt.HasTag("jane")) i++;
		}

		print ("found " + i.ToString() + " janes in billys");
    }
}
